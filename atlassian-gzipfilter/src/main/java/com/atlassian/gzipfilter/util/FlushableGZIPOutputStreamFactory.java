package com.atlassian.gzipfilter.util;

import com.atlassian.gzipfilter.flushable.FlushableGZIPOutputStream;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.zip.GZIPOutputStream;

/**
 * Factory for creating flushable GZIP output streams. Easy in JDK7+, more difficult in earlier JDKs.
 */
public class FlushableGZIPOutputStreamFactory
{
    private static final Constructor JDK7_CONSTRUCTOR;
    static
    {
        Constructor ctor = null;
        try
        {
            ctor = GZIPOutputStream.class.getConstructor(OutputStream.class, Integer.TYPE, Boolean.TYPE);
        }
        catch (NoSuchMethodException e) {
            // that's fine, we'll fall back
        }
        JDK7_CONSTRUCTOR = ctor;
    }
    public static GZIPOutputStream makeFlushableGZIPOutputStream(OutputStream out, int bufferSize) throws IOException
    {
        if (JDK7_CONSTRUCTOR == null)
        {
            return new FlushableGZIPOutputStream(out, bufferSize);
        }
        else
        {
            try {
                return (GZIPOutputStream) JDK7_CONSTRUCTOR.newInstance(out, bufferSize, true);
            } catch (InstantiationException e) {
                throw new IOException(e);
            } catch (IllegalAccessException e) {
                throw new IOException(e);
            } catch (InvocationTargetException e) {
                throw new IOException(e);
            }
        }
    }
}
