package com.atlassian.gzipfilter.util;

import java.io.*;

/**
 * Some utility methods, copied from org.apache.commons.io.IOUtils
 * <p/>
 * Copyright 2001-2004 The Apache Software Foundation.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public class IOUtils
{

    /**
     * Unconditionally close an <code>Reader</code>.
     * <p/>
     * Equivalent to {@link java.io.Reader#close()}, except any exceptions will be ignored.
     * This is typically used in finally blocks.
     *
     * @param input the Reader to close, may be null or already closed
     */
    public static void closeQuietly(Reader input)
    {
        try
        {
            if (input != null)
            {
                input.close();
            }
        }
        catch (IOException ioe)
        {
            // ignore
        }
    }

    /**
     * Unconditionally close a <code>Writer</code>.
     * <p/>
     * Equivalent to {@link java.io.Writer#close()}, except any exceptions will be ignored.
     * This is typically used in finally blocks.
     *
     * @param output the Writer to close, may be null or already closed
     */
    public static void closeQuietly(Writer output)
    {
        try
        {
            if (output != null)
            {
                output.close();
            }
        }
        catch (IOException ioe)
        {
            // ignore
        }
    }

    /**
     * Unconditionally close an <code>InputStream</code>.
     * <p/>
     * Equivalent to {@link java.io.InputStream#close()}, except any exceptions will be ignored.
     * This is typically used in finally blocks.
     *
     * @param input the InputStream to close, may be null or already closed
     */
    public static void closeQuietly(InputStream input)
    {
        try
        {
            if (input != null)
            {
                input.close();
            }
        }
        catch (IOException ioe)
        {
            // ignore
        }
    }

    /**
     * Unconditionally close an <code>OutputStream</code>.
     * <p/>
     * Equivalent to {@link java.io.OutputStream#close()}, except any exceptions will be ignored.
     * This is typically used in finally blocks.
     *
     * @param output the OutputStream to close, may be null or already closed
     */
    public static void closeQuietly(OutputStream output)
    {
        try
        {
            if (output != null)
            {
                output.close();
            }
        }
        catch (IOException ioe)
        {
            // ignore
        }
    }


}
