package com.atlassian.gzipfilter.selector;

public class NoGzipCompatibilitySelector implements GzipCompatibilitySelector
{
    public boolean shouldGzip(String contentType)
    {
        return false;
    }

    public boolean shouldGzip()
    {
        return false;
    }
}
