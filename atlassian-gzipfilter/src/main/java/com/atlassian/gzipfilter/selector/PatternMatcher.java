package com.atlassian.gzipfilter.selector;

import org.apache.commons.lang.StringUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * This class may be called by multiple threads - it is internally synchronised.
 */
public class PatternMatcher
{
    // Although synchronisation here may be a point of contention,
    // when running with 20 threads, uncached gives a performance time of
    // 27 seconds.  Cached gives 13 seconds.
    // The performance test can be found in TestPatternMatcher
    private final Map patternCache = Collections.synchronizedMap(new HashMap());

    /**
     * @param contentType     The content type to match
     * @param mimeTypesToGzip A comma separated list of regular expressions to match against
     * @return True if the content type matches a least one of the regular expressions
     */
    public boolean matches(String contentType, String mimeTypesToGzip)
    {
        final String[] mimeTypes = StringUtils.split(mimeTypesToGzip,",");
        for (int i = 0; i < mimeTypes.length; i++)
        {
            String mimeType = mimeTypes[i].trim();
            Pattern p = (Pattern) patternCache.get(mimeType);
            if (p == null)
            {
                p = Pattern.compile(mimeType);
                patternCache.put(mimeType, p);
            }

            if (p.matcher(contentType).matches())
            {
                return true;
            }
        }
        return false;
    }

}
