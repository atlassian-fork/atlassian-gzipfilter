package com.atlassian.gzipfilter;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import junit.framework.TestCase;

import com.atlassian.gzipfilter.selector.GzipCompatibilitySelector;
import com.mockobjects.dynamic.C;
import com.mockobjects.dynamic.Mock;
import com.mockobjects.servlet.MockHttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * <p>This Class currently only tests that the content-encoding header is not set when a redirect/error occurs.</p>
 * <p>todo: Write tests for most of the functionality.</p>
 */
public class TestSelectingResponseWrapper
{
    private Mock mockCompatibilitySelector;

    @Before
    public void setUp() throws Exception
    {
        mockCompatibilitySelector = new Mock(GzipCompatibilitySelector.class);
        mockCompatibilitySelector.matchAndReturn("shouldGzip", "notgzipped", false);
        mockCompatibilitySelector.matchAndReturn("shouldGzip", C.ANY_ARGS, true);
    }

    @After
    public void tearDown() throws Exception
    {
        mockCompatibilitySelector = null;
    }

    @Test
    public void testNoZipOnSpecificContent() throws IOException
    {
        HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        GzipCompatibilitySelector selector = (GzipCompatibilitySelector) mockCompatibilitySelector.proxy();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");

        selectingResponseWrapper.setContentType("notgzipped");
        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.sendRedirect("string");
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
        assertTrue(wrappedResponse.isSendRedirectCalled());
    }

    @Test
    public void testNoZipOnRedirect() throws IOException
    {
        HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        GzipCompatibilitySelector selector = (GzipCompatibilitySelector) mockCompatibilitySelector.proxy();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");

        selectingResponseWrapper.setContentType("ignoredIfNotNull");
        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.sendRedirect("string");
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
        assertTrue(wrappedResponse.isSendRedirectCalled());
    }

    @Test
    public void testNoZipOnError() throws IOException
    {
        HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        GzipCompatibilitySelector selector = (GzipCompatibilitySelector) mockCompatibilitySelector.proxy();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");

        selectingResponseWrapper.setContentType("ignoredIfNotNull");
        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.sendError(404);
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
        assertTrue(wrappedResponse.isSendErrorCalled());
    }


    /**
     * Tests that the response is not gzipped when the response code is {@link HttpServletResponse#SC_NO_CONTENT}
     */
    @Test
    public void testNoZipOnNoContentResponse()
    {
        final HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        final GzipCompatibilitySelector selector = (GzipCompatibilitySelector) mockCompatibilitySelector.proxy();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");
        selectingResponseWrapper.setContentType("ignoredIfNotNull");

        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.setStatus(HttpServletResponse.SC_NO_CONTENT);
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
    }

    /**
     * Tests that the response is not gzipped when the response code is {@link HttpServletResponse#SC_NOT_MODIFIED}
     */
    @Test
    public void testNoZipOnNotModifiedResponse()
    {
        final HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        final GzipCompatibilitySelector selector = (GzipCompatibilitySelector) mockCompatibilitySelector.proxy();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");
        selectingResponseWrapper.setContentType("ignoredIfNotNull");

        assertNull(wrappedResponse.getHeader("content-encoding"));
        selectingResponseWrapper.setStatus(HttpServletResponse.SC_NOT_MODIFIED);
        selectingResponseWrapper.finishResponse();
        assertNull(wrappedResponse.getHeader("content-encoding"));
    }

    @Test
    public void testNoActivateGzipWhenContentLengthHasBeenSetAlready()
    {
        final HeaderTrackingResponse wrappedResponse = new HeaderTrackingResponse();
        wrappedResponse.setHeader("Content-Length", "123");
        final GzipCompatibilitySelector selector = (GzipCompatibilitySelector) mockCompatibilitySelector.proxy();
        SelectingResponseWrapper selectingResponseWrapper = new SelectingResponseWrapper(wrappedResponse, selector, "");
        selectingResponseWrapper.setContentType("ignoredIfNotNull");
        selectingResponseWrapper.finishResponse();

        assertNull("If Content-Length header was set, gzip should not be activated",
                wrappedResponse.getHeader("content-encoding"));
    }

    private static class HeaderTrackingResponse extends MockHttpServletResponse
    {
        private final Map headersSet = new HashMap();
        private boolean sendRedirectCalled = false;
        private boolean sendErrorCalled = false;

        public boolean containsHeader(String key)
        {
            return headersSet.containsKey(key.toLowerCase());
        }

        public void setHeader(String s, String s1)
        {
            super.setHeader(s, s1);
            headersSet.put(s.toLowerCase(), s1);
        }

        public void addHeader(String s, String s1)
        {
            super.setHeader(s, s1);
            String existingHeader = (String) headersSet.get(s.toLowerCase());
            headersSet.put(s.toLowerCase(), existingHeader + "," + s1);
        }

        public String getHeader(String key)
        {
            return (String) headersSet.get(key.toLowerCase());
        }

        public void sendRedirect(String s) throws IOException
        {
            super.sendRedirect(s);
            sendRedirectCalled = true;
        }

        public void sendError(int anErrorCode, String anErrorMessage) throws IOException
        {
            super.sendError(anErrorCode, anErrorMessage);
            sendErrorCalled = true;
        }

        public void sendError(int anErrorCode) throws IOException
        {
            super.sendError(anErrorCode);
            sendErrorCalled = true;
        }

        public void setDateHeader(String string, long l)
        {
            // this is not implemented by Mocks, so just ignore, otherwise you get exceptions in the logs :)
        }

        public boolean isSendRedirectCalled()
        {
            return sendRedirectCalled;
        }

        boolean isSendErrorCalled()
        {
            return sendErrorCalled;
        }
    }
}
