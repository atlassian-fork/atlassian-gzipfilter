package com.atlassian.gzipfilter;

import junit.framework.TestCase;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestGzipResponseWrapper extends TestCase
{
    public void testCreateValidGzipStreamEvenWhenNothingWrittenToResponse() throws Exception
    {
        final HttpServletResponse mockHttpResponse = mock(HttpServletResponse.class);
        final ByteArrayServletOutputStream byteOutputStream = new ByteArrayServletOutputStream();
        when(mockHttpResponse.getOutputStream())
                .thenReturn(byteOutputStream);

        final GzipResponseWrapper gzipResponseWrapper = new GzipResponseWrapper(mockHttpResponse, "UTF-8");

        gzipResponseWrapper.finishResponse();

        //verify an empty but valid gzip stream was constructed
        final byte[] content = byteOutputStream.getContent();
        final GZIPInputStream gzipInputStream = new GZIPInputStream(new ByteArrayInputStream(content));
        assertThat(gzipInputStream.read(), equalTo(-1));
    }

    static class ByteArrayServletOutputStream extends ServletOutputStream
    {
        private final ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        @Override
        public void write(final int b) throws IOException
        {
            outputStream.write(b);
        }

        byte[] getContent()
        {
            return outputStream.toByteArray();
        }
    }
}
