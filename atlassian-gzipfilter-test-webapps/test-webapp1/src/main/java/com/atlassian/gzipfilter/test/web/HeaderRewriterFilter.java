package com.atlassian.gzipfilter.test.web;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * A filter that rewrites headers
 */
public class HeaderRewriterFilter implements Filter
{
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {}


    @Override
    public void doFilter(final ServletRequest req, final ServletResponse resp, final FilterChain filterChain)
            throws IOException, ServletException
    {
        filterChain.doFilter(req, resp);
        ContentTypeRewriter.serviceRequest(req, resp);
    }

    @Override
    public void destroy() {}
}
