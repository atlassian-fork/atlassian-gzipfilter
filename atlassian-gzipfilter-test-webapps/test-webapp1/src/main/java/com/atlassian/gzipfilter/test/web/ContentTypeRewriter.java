package com.atlassian.gzipfilter.test.web;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * Content type header rewriting functionality
 */
final class ContentTypeRewriter
{
    private ContentTypeRewriter(){}

    static void applyContentTypes(ServletRequest req, ServletResponse resp) {
        String mimeTypes = req.getParameter("mimetypes");
        mimeTypes = mimeTypes != null ? mimeTypes : "text/html";
        for (String newMimeType: mimeTypes.split(",")) {
            resp.setContentType(newMimeType);
        }
    }

    static void serviceRequest(ServletRequest req, ServletResponse resp) throws ServletException, IOException
    {
        applyContentTypes(req, resp);

        String outputMethod = req.getParameter("outputMethod");
        outputMethod = outputMethod != null ? outputMethod : "PrintWriter";

        PrintWriter printWriter;
        if (outputMethod.equals("PrintWriter")) {
            printWriter = resp.getWriter();
        } else {
            printWriter = new PrintWriter(resp.getOutputStream());
        }
        printWriter.write("<h1>Test Servlet</h1>");
        printWriter.close();
    }
}
